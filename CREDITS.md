Mogzol, for the amazing SCPDriverInterface which made this all possible.
https://github.com/mogzol/ScpDriverInterface

DavidRieman, for creating the compact installer for the driver itself.
https://github.com/DavidRieman/ScpDriverInterface

The Metro UI library, for the fancy UI of CC.
https://mahapps.com/

AutoUpdater, for handling the obvious thing.
https://github.com/ravibpatel/AutoUpdater.NET

Everyone on Discord for helpful feedback and ideas :)