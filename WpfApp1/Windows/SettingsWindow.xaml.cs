﻿using System.Windows;
using System.Windows.Controls;

using MahApps.Metro.Controls;

namespace CreatorsCamera
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{

		//Set the default values for settings based on user settings.
		public SettingsWindow() {
			this.Resources.Add("hCyclePathDown", Properties.Settings.Default.hCyclePathDown);
			this.Resources.Add("hCyclePathUp", Properties.Settings.Default.hCyclePathUp);
			this.Resources.Add("hPlayPath", Properties.Settings.Default.hPlayPath);
			this.Resources.Add("hStopPath", Properties.Settings.Default.hStopPath);
			this.Resources.Add("hStartRecording", Properties.Settings.Default.hStartRecording);
			this.Resources.Add("hStopRecording", Properties.Settings.Default.hStopRecording);
			this.Resources.Add("hPlayRecordedNormal", Properties.Settings.Default.hPlayRecordedNormal);
			this.Resources.Add("hPlayRecordedSSM", Properties.Settings.Default.hPlayRecordedSSM);
			InitializeComponent();



		}


		//Handle changigng of hotkeys.
		private void hCyclePathDownBox_TextChanged(object sender, TextChangedEventArgs e) {
			Properties.Settings.Default.hCyclePathDown = hCyclePathDownBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hCyclePathUpBox_TextChanged(object sender, TextChangedEventArgs e) {
			Properties.Settings.Default.hCyclePathUp = hCyclePathUpBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hStopPathBox_TextChanged(object sender, TextChangedEventArgs e) {
			Properties.Settings.Default.hStopPath = hStopPathBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hPlayPathBox_TextChanged(object sender, TextChangedEventArgs e) {
			Properties.Settings.Default.hPlayPath = hPlayPathBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hStartRecordingBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			Properties.Settings.Default.hStartRecording = hStartRecordingBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hStopRecordingBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			Properties.Settings.Default.hStopRecording = hStopRecordingBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hPlayRecordedNormalBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			Properties.Settings.Default.hPlayRecordedNormal = hPlayRecordedNormalBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}

		private void hPlayRecordedSSMBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			Properties.Settings.Default.hPlayRecordedSSM = hPlayRecordedSSMBox.Text.ToUpper();
			Properties.Settings.Default.Save();
		}
	}
}
