﻿using System.Collections.Generic;
using System.Windows;


namespace CreatorsCamera
{

	public class keyframe
	{
		public string name = "keyframe";
		//public string[] commandArray = null;
		public bool sleep = false;
		public int sleepTime = 2000;
		public bool lerp = false;
		public int new1X = 0;
		public int new1Y = 0;
		public int new2X = 0;
		public int new2Y = 0;
	}



	public partial class ScriptMaker : Window
	{

		public List<keyframe> script = new List<keyframe>();

		public ScriptMaker()
		{
			InitializeComponent();


		}

		private void AddNewKeyframe(object sender, RoutedEventArgs e)
		{

			keyframe newKeyframe = new keyframe();
			script.Add(newKeyframe);

			keyframeListUI.Items.Add(newKeyframe.name);
		}

		private void keyframeListUI_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
		{
			string name = script[keyframeListUI.SelectedIndex].name;

			kNameUI.Text = name;



		}

		private void kNameUI_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
		{
			script[keyframeListUI.SelectedIndex].name = kNameUI.Text;
		}
	}
}
