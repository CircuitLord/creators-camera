using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using ScpDriverInterface;
using AutoUpdaterDotNET;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Input;

namespace CreatorsCamera {


	//Remove later
	public class cameraScript {
		public string name;
		public string desc;
		public float versionMadeFor = 1.0f;
		public List<string[]> path = new List<string[]>();
	}




	public partial class CCNewUI : MetroWindow {

		#region VARIABLES

		public bool stopSSM = false;

		//ScpBus scpBus = new ScpBus();
		ScpBus scpBus;
		X360Controller controller = new X360Controller();
		private LowLevelKeyboardListener _listener;


		Utils utils = new Utils();

		//legacy
		public cameraScript selectedScript;

		//the path stored in memory
		public PathFile userRecPath;

		//remove???
		public int msLoopDelay = 30;

		//Good consts to have that I forget to use
		public float stickDeadzone = 8000;
		public float triggerDeadzone = 20;

		//Util vars
		public bool recordingPath = false;
		public bool addKeyFrameToRec = false;

		//The selected controller.
		public int controllerNum = 0;


		#endregion



		//Handles keypress detection.
		void _listener_OnKeyPressed(object sender, KeyPressedArgs e) {

			//Check if the user has disabled keyboard input.
			if (disableKeyboardInputBox.IsChecked == true) {
				return;
			}

			//Generate the pressed key.
			string keyPressed = utils.MakeInputStringValid(e.KeyPressed.ToString());

			//FIXME: Doesn't actually cancel the path.
			if (keyPressed == Properties.Settings.Default.hStopPath) {
				ResetAxes();
				stopSSM = true;
			}

			//Hotkey for playing camera path FIXME
			if (keyPressed == Properties.Settings.Default.hPlayPath) {
				//PlayPath(selectedScript);
			}


			//Begins recording a path.
			if (keyPressed == Properties.Settings.Default.hStartRecording) {

				if (recordingPath) {
					addKeyFrameToRec = true;
				} else {
					userRecPath = null;
					Console.WriteLine("Recording has started.");
					RecordControllerInput();
				}
			}

			//Stops recording a path.
			if (keyPressed == Properties.Settings.Default.hStopRecording) {
				recordingPath = false;
				Console.WriteLine("Recording has stopped.");
			}

			if (keyPressed == Properties.Settings.Default.hPlayRecordedNormal) {
				PlayRecordedPath("keyframe");
			}

			if (keyPressed == Properties.Settings.Default.hPlayRecordedSSM) {
				tempControllerSelectedStat.Content = "Starting SSM recording.";
				stopSSM = false;
				Console.WriteLine("Playing path.");
				PlayRecordedPath("sstester");
			}

			//if (keyPressed == "P") {
			//	PlayRecordedPath("sstester");
			//}

		}

		async void RecordControllerInput(string type = "old") {

			if (type == "old") {

				if (recordingPath) {
					MessageBox.Show("You're already recording a path.");
					return;
				}

				recordingPath = true;

				int timesRecorded1 = 0;
				int timesRecorded2 = 0;
				int timesRecordedLT = 0;
				int timesRecordedRT = 0;


				double totalTurn = 0;

				bool moved = false;
				PathFile path = new PathFile();

				PathKeyframe keyframe = new PathKeyframe();


				//TODO: Add a naming thingy
				path.id = "1234";
				path.name = "RandomName";


				//Start recording the path.
				while (recordingPath) {

					if (addKeyFrameToRec) {

						//Calculate avergage move speeds.

						keyframe.avg1X = utils.testValid(keyframe.total1X / timesRecorded1);
						keyframe.avg1Y = utils.testValid(keyframe.total1Y / timesRecorded1);
						keyframe.avg2X = utils.testValid(keyframe.total2X / timesRecorded2);
						keyframe.avg2Y = utils.testValid(keyframe.total2Y / timesRecorded2);
						keyframe.avgLTrigger = keyframe.totalLTrigger / timesRecordedLT;
						keyframe.avgRTrigger = keyframe.totalRTrigger / timesRecordedRT;

						//Add the keyframe to the list.
						path.keyframes.Add(keyframe);

						//Reset the keyframe for the next loop.
						keyframe.total1X = 0;
						keyframe.total1Y = 0;
						keyframe.total2X = 0;
						keyframe.total2Y = 0;
						keyframe.totalLTrigger = 0;
						keyframe.totalRTrigger = 0;
						timesRecorded1 = 0;
						timesRecorded2 = 0;
						timesRecordedLT = 0;
						timesRecordedRT = 0;

						keyframe = new PathKeyframe();
						Console.WriteLine("keyframe added");

						//Make sure this doesn't run again.
						addKeyFrameToRec = false;

						continue;
					}


					await Task.Delay(100);
					float curr1X = ControllerInput.XboxController.RetrieveController(controllerNum).LeftThumbStick.X;

					tempControllerSelectedStat.Content = "Controller 1X Value: " + curr1X;

					float curr1Y = ControllerInput.XboxController.RetrieveController(controllerNum).LeftThumbStick.Y;
					float curr2X = ControllerInput.XboxController.RetrieveController(controllerNum).RightThumbStick.X;
					double temp = (utils.testValid(curr2X)) * 0.001;
					totalTurn += temp;
					Console.WriteLine(totalTurn);
					float curr2Y = ControllerInput.XboxController.RetrieveController(controllerNum).RightThumbStick.Y;
					float currLTrigger = ControllerInput.XboxController.RetrieveController(controllerNum).LeftTrigger;
					float currRTrigger = ControllerInput.XboxController.RetrieveController(controllerNum).RightTrigger;

					//If any of the sticks are moving above the deadzone radius.
					if (curr1X > stickDeadzone || curr1Y > stickDeadzone || curr1X < -stickDeadzone || curr1Y < -stickDeadzone) {
						//Console.WriteLine(curr1X);
						timesRecorded1++;
						keyframe.total1X += curr1X;
						keyframe.total1Y += curr1Y;

						moved = true;
					}

					if (curr2X > stickDeadzone || curr2Y > stickDeadzone || curr2X < -stickDeadzone || curr2Y < -stickDeadzone) {
						timesRecorded2++;
						keyframe.total2X += curr2X;
						keyframe.total2Y += curr2Y;

						moved = true;
					}

					if (currLTrigger > triggerDeadzone) {
						timesRecordedLT++;
						keyframe.totalLTrigger += currLTrigger;

						moved = true;
					}

					if (currRTrigger > triggerDeadzone) {
						timesRecordedRT++;
						keyframe.totalRTrigger += currRTrigger;

						moved = true;
					}


					if (moved) {
						keyframe.totalTime += 70;
					}

				}
				//Recording done.
				tempControllerSelectedStat.Content = "Recording Complete.";

				userRecPath = path;

			}


		}

		async void PlayRecordedPath(string type) {

			if (type == "keyframe") {

				if (userRecPath == null) {
					MessageBox.Show("You need to record a path before you can play it back! KEYFRAME");
					return;
				}

				int totalKeyframes = userRecPath.keyframes.Count;
				int currKeyframe = 0;

				foreach (PathKeyframe keyframe in userRecPath.keyframes) {

					Console.WriteLine(keyframe.avg1X);

					controller.LeftStickX = (short)keyframe.avg1X;
					controller.LeftStickY = (short)keyframe.avg1Y;
					controller.RightStickX = (short)keyframe.avg2X;
					controller.RightStickY = (short)keyframe.avg2Y;
					controller.LeftTrigger = (byte)keyframe.avgLTrigger;
					controller.RightTrigger = (byte)keyframe.avgRTrigger;
					SetAxes();

					//If this is the last keyframe in the list, don't lerp to the next one.
					if ((currKeyframe + 1) == totalKeyframes) {
						await Task.Delay((int)keyframe.totalTime);
						continue;
					}

					//Else:



					/*

					if (keyframe.totalTime < 1500) {
						lerpTime = keyframe.totalTime * 0.5;
					}

					await Task.Delay((int)(keyframe.totalTime - lerpTime));

					//Time to start lerping to the next keyframe-value.

					//float tempPos = Lerp(keyframe.avg1X, userRecPath.keyframes[currKeyframe + 1], lerpT);

					double timesToLoop = lerpTime / msLoopDelay;
					for (int i = 0; i < timesToLoop; i++) {
						double lerpT = (1 / timesToLoop) * i;
						double tempPos1X = Lerp((float)keyframe.avg1X, (float)userRecPath.keyframes[currKeyframe + 1].avg1X, (float)lerpT);
						double tempPos1Y = Lerp((float)keyframe.avg1Y, (float)userRecPath.keyframes[currKeyframe + 1].avg1Y, (float)lerpT);
						double tempPos2X = Lerp((float)keyframe.avg2X, (float)userRecPath.keyframes[currKeyframe + 1].avg2X, (float)lerpT);
						double tempPos2Y = Lerp((float)keyframe.avg2Y, (float)userRecPath.keyframes[currKeyframe + 1].avg2Y, (float)lerpT);
						double tempPosLTrigger = Lerp((float)keyframe.avgLTrigger, (float)userRecPath.keyframes[currKeyframe + 1].avgLTrigger, (float)lerpT);
						double tempPosRTrigger = Lerp((float)keyframe.avgRTrigger, (float)userRecPath.keyframes[currKeyframe + 1].avgRTrigger, (float)lerpT);

						controller.LeftStickX = (short)utils.ApplyMultiplier(tempPos1X, 1);
						controller.LeftStickY = (short)utils.ApplyMultiplier(tempPos1Y, 1);
						controller.RightStickX = (short)utils.ApplyMultiplier(tempPos2X, 1);
						controller.RightStickY = (short)utils.ApplyMultiplier(tempPos2Y, 1);
						controller.LeftTrigger = (byte)tempPosLTrigger;
						controller.RightTrigger = (byte)tempPosRTrigger;
						SetAxes();

						await Task.Delay(msLoopDelay * 2);
					}

					*/




					currKeyframe++;

				}

				ResetAxes();
			}

			if (type == "sstester") {

				if (userRecPath == null) {
					MessageBox.Show("You need to record a path before you can play it back!");
					return;
				}

				Directory.CreateDirectory("pictures");
				Directory.CreateDirectory("renders");

				//Make folder for screenshots.
				string folderPath = DateTime.Now.ToString().Replace("/", "-").Replace(":", "-");
				Directory.CreateDirectory("pictures\\" + folderPath);

				//Init the screencapture;
				ScreenCapture sc = new ScreenCapture();


				foreach (PathKeyframe keyframe in userRecPath.keyframes) {

					int timesToLoop = Int32.Parse(ssRenderFramesBox.Text);
					int timeToWait = Int32.Parse(ssRenderWaitTimeBox.Text);

					double amtMove1X = 0, amtMove1Y = 0, amtMove2X = 0, amtMove2Y = 0;

					amtMove1X = utils.FindLowerAvg(keyframe.avg1X);
					amtMove1Y = utils.FindLowerAvg(keyframe.avg1Y);
					amtMove2X = utils.FindLowerAvg(keyframe.avg2X);
					amtMove2Y = utils.FindLowerAvg(keyframe.avg2Y);


					if (timesToLoop == 0) {
						//Divide the total MS by the framerate.
						timesToLoop = (int)Math.Round(keyframe.totalTime / 12, 0);
					}

					bool autoRefocus = (bool)enableAutoRefocusInputBox.IsChecked;

					int SSMMoveTime = Int32.Parse(UISSMMoveTime.Text);

					//Actually start moving.
					for (int i = 0; i < timesToLoop; i++) {

						if (stopSSM) {
							tempControllerSelectedStat.Content = "SSM Canceled. :(";
							stopSSM = false;
							return;
						}

						controller.LeftStickX = (short)amtMove1X;
						controller.LeftStickY = (short)amtMove1Y;
						controller.RightStickX = (short)amtMove2X;
						controller.RightStickY = (short)amtMove2Y;
						controller.LeftTrigger = 0;
						controller.RightTrigger = 0;
						SetAxes();

						await Task.Delay(SSMMoveTime);
						ResetAxes();
						if (autoRefocus) {
							controller.Buttons |= X360Buttons.X;
							SetAxes();
						}
						await Task.Delay(timeToWait);
						controller.Buttons &= ~X360Buttons.X;

						//ScreenCapture sc = new ScreenCapture();
						sc.CaptureScreenToFile("pictures/" + folderPath + "/" + i.ToString() + ".png", ImageFormat.Png);


					}
				}

				tempControllerSelectedStat.Content = "SSM Finished!";
			}

		}

		//Initalize the UI.
		public CCNewUI() {

			//Set the title of the program.
			Version version = Assembly.GetEntryAssembly().GetName().Version;
			this.Resources.Add("titleBarText", "Creator's Camera v" + version.ToString() + " - by CircuitLord");

			InitializeComponent();

			//Load the ListBox UI and select the first script in the list.
			//ReloadScripts();
			//if (scriptsListUI.HasItems)
			//	scriptsListUI.SelectedIndex = 0;

			controllerToUseUI.Items.Add("0");
			controllerToUseUI.Items.Add("1");
			controllerToUseUI.Items.Add("2");
			controllerToUseUI.SelectedIndex = 1;


			//Initialize the keyboard listener.
			_listener = new LowLevelKeyboardListener();
			_listener.OnKeyPressed += _listener_OnKeyPressed;
			_listener.HookKeyboard();

			//Plugin the virtual controller.
			//And see if driver is installed.


			try {
				scpBus = new ScpBus();
				scpBus.PlugIn(1);
			} catch {
				MessageBoxResult result = MessageBox.Show("It doesn't look like you have the required driver installed.\nClick OK to run the driver installer now, or Cancel to cancel.", "Creator's Camera", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

				//If the user clicks OK, run the SCPDriverInstaller.
				if (result == MessageBoxResult.OK) {
					utils.ExecuteAsAdmin(Directory.GetCurrentDirectory() + "/DriverInstaller/SCPDriverInstaller.exe");
					Environment.Exit(1);
				}

				if (result == MessageBoxResult.Cancel) {
					//Environment.Exit(1);
				}

			}

			//Start polling the user's controller.
			ControllerInput.XboxController.StartPolling();

			//Check for updates		
			AutoUpdater.Start("https://circuitcubed.com/creatorscamera/newestRelease.xml");



		}


		//Opens the settings window.
		private void ShowSettings(object sender, RoutedEventArgs e) {
			SettingsWindow settingsWindow = new SettingsWindow();
			settingsWindow.Show();
		}

		#region Controller_Functions
		//Resets values of all sticks.
		void ResetAxes() {
			controller.RightStickX = 0;
			controller.RightStickY = 0;
			controller.LeftStickX = 0;
			controller.LeftStickY = 0;
			controller.LeftTrigger = 0;
			controller.RightTrigger = 0;

			//Update the virtual sticks.
			SetAxes();
		}

		//Submits set values of sticks.
		void SetAxes() {
			scpBus.Report(1, controller.GetReport());
			controllerStatsUI.Clear();
			controllerStatsUI.AppendText("stick1X: " + controller.LeftStickX + "\nstick1Y: " + controller.LeftStickY + "\n");
			controllerStatsUI.AppendText("stick2X: " + controller.RightStickX + "\nstick2Y: " + controller.RightStickY + "\n");

		}

		#endregion




		//Handle exiting.
		private void CCNewUI_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			ResetAxes();
			ControllerInput.XboxController.StopPolling();
			_listener.UnHookKeyboard();

			//scpBus.UnplugAll();

		}

		//TODO:
		//Removed slider functions, replace all in the apply multiplier function eventually

		//stick1Multi = Convert.ToSingle(stick1MultiSliderUI.Value);

		//Reset the multipliers when button clicked.
		private void resetMultiButtonUI_Click(object sender, RoutedEventArgs e) {
			stick1MultiSliderUI.Value = 1;
			stick2MultiSliderUI.Value = 1;
		}


		//The documentation open button.
		private void Button_Click(object sender, RoutedEventArgs e) {
			System.Diagnostics.Process.Start("https://creators-camera.gitbook.io/project");
		}


		//Sets the selected controller for the user.
		private void controllerToUseUI_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			controllerNum = Int32.Parse(controllerToUseUI.SelectedItem.ToString());
		}


	}

}
