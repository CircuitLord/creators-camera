﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ScpDriverInterface;

namespace CreatorsCamera {

	public partial class MainWindow : Window {

		ScpBus scpBus = new ScpBus();
		X360Controller controller = new X360Controller();
		private LowLevelKeyboardListener _listener;
		public cameraScript selectedScript;

		//Future feature
		public float moveAxisMulti = 1.0f; //Movement multiplier
		public float turnAxisMulti = 1.0f; //Turning multiplier
		public float zAxisMulti = 1.0f; //Up and down multiplier

		public string playPathHotkey = "F";

		public float version = 1.0f;

		//Initialize all stuff in window by default.
		public MainWindow() {
			InitializeComponent();
			this.Closed += new EventHandler(MainWindow_Closed);

			//Update needed text boxes.

			List<string> scriptsList = new List<string>();
			foreach (string file in Directory.EnumerateFiles(".\\paths", "*.txt")) {
				string filename = file.ToString().Replace(".\\paths\\", "").Replace(".txt", "");
				scriptsList.Add(filename);
			}
			scriptsListUI.ItemsSource = scriptsList;

			List<string> hotkeyPlaySelectedOptions = new List<string>(); {
				hotkeyPlaySelectedOptions.Add("f");
				hotkeyPlaySelectedOptions.Add("c");
				hotkeyPlaySelectedOptions.Add("g");
				hotkeyPlaySelectedOptions.Add("v");
			}

			hotkeyPlaySelectedUI.ItemsSource = hotkeyPlaySelectedOptions;

			//Initialize the keyboard listener.
			_listener = new LowLevelKeyboardListener();
			_listener.OnKeyPressed += _listener_OnKeyPressed;
			_listener.HookKeyboard();

		}

		//Button to setup/connect controller 1.
		private void ConnectController(object sender, RoutedEventArgs e) {
			scpBus.PlugIn(1);
			statsUI1.Text = "Connected";
			//Reset the sticks.
			ResetAxes();
		}

		//Selection changed in Paths List.
		private void PathSelected(object sender, SelectionChangedEventArgs e) {
			string currSelected = scriptsListUI.SelectedItem.ToString();
			cameraScript script = LoadScriptFromDisk(currSelected);

			scriptDescUI.Clear();
			scriptDescUI.AppendText("NAME: " + script.name + "\n");
			scriptDescUI.AppendText("DESC: " + script.desc + "\n");

			string versionGood;
			if (version - script.versionMadeFor >= 0) {
				versionGood = "Looks good!";
			} else {
				versionGood = "This may not work.";
			}

			scriptDescUI.AppendText("VERSION: This script was designed for v" + script.versionMadeFor + ". " + versionGood + "\n");

			string pathList = "";
			scriptDescUI.AppendText("PATH: \n" + "Doesn't work yet.");

			selectedScript = script;

		}

		async void PlayPath(cameraScript script) {

			for (int i = 0; i < script.path.Count; i++) {

				string line = script.path[i][0].ToLower();
				Console.WriteLine(line);

				if (line == "lerp1x") {
					Lerp1X(Convert.ToSingle(script.path[i][1]), Convert.ToSingle(script.path[i][2]));
				} else if (line == "lerp1y") {
					Lerp1Y(Convert.ToSingle(script.path[i][1]), Convert.ToSingle(script.path[i][2]));
				} else if (line == "lerp2x") {
					Lerp2X(Convert.ToSingle(script.path[i][1]), Convert.ToSingle(script.path[i][2]));
				} else if (line == "lerp2y") {
					Lerp2Y(Convert.ToSingle(script.path[i][1]), Convert.ToSingle(script.path[i][2]));
				} else if (line == "sleep") {
					await Task.Delay(Convert.ToInt32(script.path[i][1]));
				} else if (line == "set1x") {
					controller.LeftStickX = Convert.ToInt16(script.path[i][1]);
				} else if (line == "set1y") {
					controller.LeftStickY = Convert.ToInt16(script.path[i][1]);
				} else if (line == "set2x") {
					controller.RightStickX = Convert.ToInt16(script.path[i][1]);
				} else if (line == "set2y") {
					controller.RightStickY = Convert.ToInt16(script.path[i][1]);
				} else if (line == "resetall") {
					ResetAxes();
				}

				SetAxes();

			}

		}

		public class cameraScript {
			public string name;
			public string desc;
			public float versionMadeFor = 1.0f;
			public List<string[]> path = new List<string[]>();
		}

		//Handles detection of keypresse
		void _listener_OnKeyPressed(object sender, KeyPressedArgs e) {

			string keyPressed = e.KeyPressed.ToString();

			if (keyPressed == "T") {
				ResetAxes();

			}

			if (keyPressed == playPathHotkey) {
				PlayPath(selectedScript);
			}

		}

		//FUNCTIONS

		//Load a script into memory.
		cameraScript LoadScriptFromDisk(string scriptName) {
			cameraScript script = new cameraScript();

			TextReader textFile = new StreamReader("./paths/" + scriptName + ".txt");

			string line;
			int i = -1;
			while ((line = textFile.ReadLine()) != null) {
				i++;
				//Ignore blank lines.
				if (string.IsNullOrEmpty(line)) {
					continue;
				}

				//Set the properties of the cameraScript
				if (line.StartsWith("name:")) {
					script.name = line.Substring(6);
					continue;
				} else if (line.StartsWith("desc:")) {
					script.desc = line.Substring(6);
					continue;
				} else if (line.StartsWith("versionMadeFor:")) {
					script.versionMadeFor = Convert.ToSingle(line.Substring(16));
					continue;
				}

				//Time to read the path.
				string[] lineArray = line.Split(' ');
				script.path.Add(lineArray);

			}

			return script;
		}

		//Utility math function.
		float Lerp(float startPos, float endPos, float t) {
			return startPos * (1 - t) + endPos * t;
		}

		//Lerps between two axis values smoothly.
		async void Lerp1X(float newStickPos = 0, float t = 2) {
			int msLoopDelay = 20;
			float timesToLoop = (t * 1000) / msLoopDelay;
			float oldStickPos = controller.LeftStickX;

			for (int i = 0; i < timesToLoop; i++) {

				float lerpT = (1 / timesToLoop) * i;
				float tempPos = Lerp(oldStickPos, newStickPos, lerpT);
				short tempPos2 = Convert.ToInt16(tempPos);

				controller.LeftStickX = tempPos2;
				SetAxes();

				await Task.Delay(msLoopDelay);
			}
		}

		//Lerps between two axis values smoothly.
		async void Lerp1Y(float newStickPos = 0, float t = 2) {
			int msLoopDelay = 20;
			float timesToLoop = (t * 1000) / msLoopDelay;
			float oldStickPos = controller.LeftStickY;

			for (int i = 0; i < timesToLoop; i++) {

				float lerpT = (1 / timesToLoop) * i;
				float tempPos = Lerp(oldStickPos, newStickPos, lerpT);
				short tempPos2 = Convert.ToInt16(tempPos);

				controller.LeftStickY = tempPos2;
				SetAxes();

				await Task.Delay(msLoopDelay);
			}
		}

		//Lerps between two axis values smoothly.
		async void Lerp2X(float newStickPos = 0, float t = 2) {
			int msLoopDelay = 20;
			float timesToLoop = (t * 1000) / msLoopDelay;
			float oldStickPos = controller.RightStickX;
			Console.WriteLine(oldStickPos);

			for (int i = 0; i < timesToLoop; i++) {

				float lerpT = (1 / timesToLoop) * i;
				float tempPos = Lerp(oldStickPos, newStickPos, lerpT);
				short tempPos2 = Convert.ToInt16(tempPos);

				controller.RightStickX = tempPos2;
				SetAxes();

				await Task.Delay(msLoopDelay);
			}
		}

		//Lerps between two axis values smoothly.
		async void Lerp2Y(float newStickPos = 0, float t = 2) {
			int msLoopDelay = 20;
			float timesToLoop = (t * 1000) / msLoopDelay;
			float oldStickPos = controller.RightStickY;

			for (int i = 0; i < timesToLoop; i++) {

				float lerpT = (1 / timesToLoop) * i;
				float tempPos = Lerp(oldStickPos, newStickPos, lerpT);
				short tempPos2 = Convert.ToInt16(tempPos);

				controller.RightStickY = tempPos2;
				SetAxes();

				await Task.Delay(msLoopDelay);
			}
		}

		async void LerpLTrigger(float newStickPos = 0, float t = 2) {
			int msLoopDelay = 20;
			float timesToLoop = (t * 1000) / msLoopDelay;
			float oldStickPos = controller.LeftTrigger;

			for (int i = 0; i < timesToLoop; i++) {

				float lerpT = (1 / timesToLoop) * i;
				float tempPos = Lerp(oldStickPos, newStickPos, lerpT);
				byte tempPos2 = Convert.ToByte(tempPos);

				controller.LeftTrigger = tempPos2;
				SetAxes();

				await Task.Delay(msLoopDelay);
			}
		}

		async void LerpRTrigger(float newStickPos = 0, float t = 2) {
			int msLoopDelay = 20;
			float timesToLoop = (t * 1000) / msLoopDelay;
			float oldStickPos = controller.RightTrigger;

			for (int i = 0; i < timesToLoop; i++) {

				float lerpT = (1 / timesToLoop) * i;
				float tempPos = Lerp(oldStickPos, newStickPos, lerpT);
				byte tempPos2 = Convert.ToByte(tempPos);

				controller.RightTrigger = tempPos2;
				SetAxes();

				await Task.Delay(msLoopDelay);
			}
		}

		//Resets values of all sticks.
		void ResetAxes() {
			controller.RightStickX = 0;
			controller.RightStickY = 0;
			controller.LeftStickX = 0;
			controller.LeftStickY = 0;

			//Update the virtual sticks.
			SetAxes();
		}

		//Submits set values of sticks.
		void SetAxes() {
			scpBus.Report(1, controller.GetReport());
			statsUI2.Text = "stick1X: " + controller.RightStickX.ToString();
			statsUI3.Text = "stick1Y: " + controller.RightStickY.ToString();
			statsUI4.Text = "stick2X: " + controller.LeftStickX.ToString();
			statsUI5.Text = "stick2Y: " + controller.LeftStickY.ToString();
		}

		//Handle exiting.
		void MainWindow_Closed(object sender, EventArgs e) {
			_listener.UnHookKeyboard();
			scpBus.UnplugAll();
		}

		private void hotkeyPlaySelectedUI_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			Console.WriteLine(hotkeyPlaySelectedUI.SelectedItem.ToString() + ".");

			playPathHotkey = hotkeyPlaySelectedUI.SelectedItem.ToString().ToUpper();
		}
	}
}