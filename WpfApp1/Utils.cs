﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CreatorsCamera {

	class Utils {

		public string MakeInputStringValid(string keyPressed) {
			keyPressed = keyPressed.Replace("Subtract", "-");
			keyPressed = keyPressed.Replace("Add", "+");
			keyPressed = keyPressed.Replace("Divide", "/");
			keyPressed = keyPressed.Replace("Multiply", "*");
			keyPressed = keyPressed.Replace("Multiply", "*");
			keyPressed = keyPressed.Replace("OemPlus", "=");
			keyPressed = keyPressed.Replace("D1", "1");
			keyPressed = keyPressed.Replace("D2", "2");
			keyPressed = keyPressed.Replace("D3", "3");
			keyPressed = keyPressed.Replace("D4", "4");
			keyPressed = keyPressed.Replace("D5", "5");
			keyPressed = keyPressed.Replace("D6", "6");
			keyPressed = keyPressed.Replace("D7", "7");
			keyPressed = keyPressed.Replace("D8", "8");
			keyPressed = keyPressed.Replace("D9", "9");
			keyPressed = keyPressed.Replace("D0", "0");
			keyPressed = keyPressed.Replace("OemComma", ",");
			keyPressed = keyPressed.Replace("OemPeriod", ".");
			keyPressed = keyPressed.Replace("OemOpenBrackets", "[");
			keyPressed = keyPressed.Replace("Oem6", "]");
			keyPressed = keyPressed.Replace("Oem1", ";");
			keyPressed = keyPressed.Replace("OemQuotes", "'");
			keyPressed = keyPressed.Replace("Oem3", "`");
			keyPressed = keyPressed.Replace("OemQuestion", "?");

			return keyPressed;
		}

		public void ExecuteAsAdmin(string fileName) {
			Process proc = new Process();
			proc.StartInfo.FileName = fileName;
			proc.StartInfo.UseShellExecute = true;
			proc.StartInfo.Verb = "runas";
			proc.Start();
		}


		public double FindLowerAvg(double x) {
			if (x > 9000) {
				return x - ((x - 9000) * 0.8);
			} else if (x < -9000) {
				return x - ((x + 9000) * 0.8);
			} else
				return 0;
		}


		public double testValid(double value) {
			if (Math.Abs(value) < 8000) {
				return 0;
			} else {
				return value;
			}
		}


		//Utility math function.
		public double Lerp(float startPos, float endPos, float t) {
			return startPos * (1 - t) + endPos * t;
		}

		public double ApplyMultiplier(double value, int id = 1) {

			double newValue = 0;
			double stick1Multi = 1;
			double stick2Multi = 1f;

			//Multiply the value by the stick multiplier.
			//Stick 1
			if (id == 1) {
				newValue = value * stick1Multi;
				//Stick2
			} else {
				newValue = value * stick2Multi;
			}

			//More sticks?!?!

			//Make sure the new value isn't too big or too small to be valid.
			if (newValue > 32767f) {
				newValue = 32767f;
			} else if (newValue < -32766f) {
				newValue = -32766f;
			}

			return newValue;
		}

		public double CheckValueStick(double x) {
			if (x > 9000)
				return 9000;

			if (x < -9000)
				return -9000;

			else return 0;
		}




	}
}
