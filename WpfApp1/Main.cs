﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreatorsCamera {

	



	class Main {

		Utils utils = new Utils();
		private LowLevelKeyboardListener _listener;

		public bool keyboardEnabled = true;


		//Change if the keyboard is enabled or not.
		public void ToggleKeyboardEnabled(bool newState) {
			keyboardEnabled = newState;
		}

		//Init the keyboard listener.
		public void InitKeyboardListener() {
			_listener = new LowLevelKeyboardListener();
			_listener.OnKeyPressed += _listener_OnKeyPressed;
			_listener.HookKeyboard();
		}


		//Listens for keypresses.
		void _listener_OnKeyPressed(object sender, KeyPressedArgs e) {
			string keyPressed = utils.MakeInputStringValid(e.KeyPressed.ToString());

		}


	}
}
