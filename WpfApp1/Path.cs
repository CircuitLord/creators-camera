﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreatorsCamera
{



	//A path.
    public class PathFile {

		public List<PathKeyframe> keyframes = new List<PathKeyframe>();
		public string id;
		public string name;
	



    }



	public class ListPathKeyframe3D {
		public List<PathKeyframe3D> keyframes = new List<PathKeyframe3D>();
		public string id;
		public string name;
	}

	public class PathKeyframe3D {
		public double x;
		public double y;
		public double z;
		public double rotationH;
		public double rotationV;
	}

	public class Camera3D {
		public double x;
		public double y;
		public double z;
		public double rotationH;
		public double rotationV;
	}


	public class PathKeyframe {
		public double totalTime = 0;

		public double total1X = 0;
		public double total1Y = 0;
		public double total2X = 0;
		public double total2Y = 0;

		public double avg1X = 0;
		public double avg1Y = 0;
		public double avg2X = 0;
		public double avg2Y = 0;

		public double totalLTrigger = 0;
		public double totalRTrigger = 0;

		public double avgLTrigger = 0;
		public double avgRTrigger = 0;
	}



}
